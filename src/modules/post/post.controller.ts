import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { PostService } from './post.service';
import {
  CreatePostDto,
  CurrentUser,
  GetAllPostsDto,
  JwtAuthGuard,
  UpdatePostDto,
} from 'src/core';

@Controller('post')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @Post()
  createPost(@Body() data: CreatePostDto, @CurrentUser() user) {
    return this.postService.createNewPost(data, user.id);
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  getPosts(@Query() data: GetAllPostsDto, @Req() req?: any) {
    // console.log(req)
    const { limit, page, search } = data;
    return this.postService.getAllPosts({
      limit: Number(limit),
      page: Number(page),
      term: search,
    });
  }

  @Put(':id')
  updatePosts(@Param('id') id: number, @Body() data: UpdatePostDto) {
    return this.postService.updatePost(id, data);
  }

  @Get(':id')
  getPost(@Param('id') id: number) {
    return this.postService.getOnePost(id);
  }
}
