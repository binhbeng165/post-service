import { Logger, Provider } from '@nestjs/common';
import Redis from 'ioredis';
import { ConfigService } from '@nestjs/config';

export type RedisClient = Redis;
const logger = new Logger();
export const RedisProvider: Provider = {
  provide: 'REDIS_CLIENT',
  useFactory: (configService: ConfigService): RedisClient => {
    const redisConfig = {
      host: configService.get('REDIS_HOST', 'localhost'),
      port: configService.get('REDIS_PORT', 6379),
      db: 2,
      keyPrefix: 'post_service',
    };
    const redisClient = new Redis(redisConfig);

    redisClient.on('connect', () => {
      logger.log(`🟢 Connected to Redis`);
    });

    redisClient.on('error', (err) => {
      console.error('🔴 Error connecting to Redis:', err);
    });

    return redisClient;
  },
  inject: [ConfigService],
};
