export const config = (): Config => ({
  node_env: process.env.NODE_ENV,
  port: parseInt(process.env.PORT, 10) || 3000,

  jwt: {
    secret: process.env.JWT_SECRET || 'default-secret',
    expire: parseInt(process.env.JWT_EXPIRE, 10) || 1000,
  },
  redis: {
    host: process.env.REDIS_HOST || 'localhost',
    port: parseInt(process.env.REDIS_PORT, 10) || 6379,
    password: process.env.REDIS_PASSWORD || '',
    db: parseInt(process.env.REDIS_DB, 10) || 0,
    prefix: process.env.REDIS_PREFIX || 'post_service',
  },
  rabbitmq: {
    url: process.env.RABBITMQ_URL || 'amqp://guest:guest@localhost:5672',
    service: {
      auth: process.env.AUTH_QUEUE || 'default-auth-queue',
      post: process.env.POST_QUEUE || 'default-post-queue',
    },
  },
});

interface Config {
  node_env: string | undefined;
  port: number;
  jwt: {
    secret: string;
    expire: number;
  };
  redis: {
    host: string;
    port: number;
    password?: string;
    db?: number;
    prefix?: string;
  };
  rabbitmq: {
    url: string;
    service: {
      auth: string;
      post: string;
    };
  };
}
